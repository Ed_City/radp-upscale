<?php include "inc/header.php";?>

<!--Home Main Carousel-->
<section class="main-slider-hero">

  <!-- Loop each slider component inside here. -->
  <!--Slider-->
  <div class="carousel-cell">
    <div class="hero" style="background-image: url('assets/images/home-hero-banner.png');">
      <div class="container">
        <div class="col-lg-7">
          <h1 class="title">
            Users love us, brands trust us and so can you.
          </h1>
        </div>
      </div>
    </div>
  </div>
  <!-- / Slider-->

  <!--Slider-->
  <div class="carousel-cell">
    <div class="hero" style="background-image: url('assets/images/home-hero-banner-2.png');">
      <div class="container">
        <div class="col-lg-7">
          <h1 class="title">
            We enable brand growth through creativity, media reach and digital expertise across Africa
          </h1>
        </div>
      </div>
    </div>
  </div>
  <!-- / Slider-->

  <!--Slider-->
  <div class="carousel-cell">
    <div class="hero" style="background-image: url('assets/images/home-hero-banner-3.png');">
      <div class="container">
        <div class="col-lg-7">
          <h1 class="title">
            We inform, entertain and engage millions of users across Sub-Saharan Africa
          </h1>
        </div>
      </div>
    </div>
  </div>
  <!-- / Slider-->

  <!--Slider-->
  <div class="carousel-cell">
    <div class="hero" style="background-image: url('assets/images/home-hero-banner-4.png');">
      <div class="container">
        <div class="col-lg-7">
          <h1 class="title">
            Helping brands share meaningful stories with their audience
          </h1>
        </div>
      </div>
    </div>
  </div>
  <!-- / Slider-->

  <!--Slider-->
  <div class="carousel-cell">
    <div class="hero" style="background-image: url('assets/images/home-hero-banner-5.png');">
      <div class="container">
        <div class="col-lg-7">
          <h1 class="title">
            And creating meaningful connections between customers and brands.
          </h1>
        </div>
      </div>
    </div>
  </div>
  <!-- / Slider-->

</section>
<!-- / Home Main Carousel-->

<!-- Linted or Highlight section -->
<section class="container-fluid highlight-section">
  <div class="col-md-10 mx-auto">
    <div class="row">
      <div class="col-md-8">
        <blockquote>
          Having mastered that point where creativity intersects with digital technologies <span class="pink-text">we
            help brands connect</span> with a rapidly evolving African audience.
        </blockquote>
      </div>
    </div>
  </div>
</section>
<!-- / Linted or Highlight section -->

<!-- Success Stories -->
<section class="section-padding bg-white">

  <div class="container">
    <!-- Heading -->
    <h1 class="heading">
      Success Stories
    </h1>
    <!-- / Heading -->

    <!-- Case Studies Mini Slider -->
    <div class="case-studies">

      <!-- Case Study -->
      <div class="col-lg-4 col-md-5 col-8">
        <div class="card">
          <img src="assets/images/milo-placeholder.png" class="card-img" alt="Milo Case Study">
          <div class="card-img-overlay">
            MILO - RTD Launch Campaign
          </div>
          <a class="stretched-link" href="/pages/case-study-template.php"></a>
        </div>
      </div>
      <!-- / Case Study -->

      <!-- Case Study -->
      <div class="col-lg-4 col-md-5 col-8">
        <div class="card">
          <img src="assets/images/durex-placeholder.png" class="card-img" alt="Durex - Perfoma Party">
          <div class="card-img-overlay">
            Durex - Perfoma Party
          </div>
          <a class="stretched-link" href="/pages/case-study-template.php"></a>
        </div>
      </div>
      <!-- / Case Study -->

      <!-- Case Study -->
      <div class="col-lg-4 col-md-5 col-8">
        <div class="card">
          <img src="assets/images/heineken-placeholder.png" class="card-img"
            alt="Heineken UEFA Champions League Social Media Campaign">
          <div class="card-img-overlay">
            Heineken UEFA Champions League Social Media Campaign
          </div>
          <a class="stretched-link" href="/pages/case-study-template.php"></a>
        </div>
      </div>
      <!-- / Case Study -->

    </div>
    <!-- / Case Studies Mini Slider -->

    <div class="text-center">
      <a href="/pages/case-study-archive.php">Explore more</a>
    </div>

  </div>

</section>
<!-- / Success Stories -->

<!-- Product Overview -->
<section class="bg-white">
  <div class="container radp-grey-bg">

    <!-- Header -->
    <h1 class="heading">
      Digitally Harnessing Creativity
    </h1>
    <!-- / Header -->

    <!-- Header Description -->
    <article class="heading-description">
      We’re experts at engaging digital audiences. Through storytelling, strategy and our industry-leading media
      network, we empower brands with digital solutions.
    </article>
    <!-- / Header Description -->

    <!-- Products -->
    <div class="row products">

      <!-- Product Column -->
      <div class="col-md-4">

        <div class="position-relative h-100">
          <!-- Product -- Shows extra detail on hover only on desktop -->
          <div class="product pulse">

            <!-- Product Image -->
            <div class="product-image">
              <img class="img-fluid" src="assets/images/pulse-logo.png" alt="Pulse" />
            </div>
            <!-- / Product Image -->

            <!-- Product Icon -->
            <div class="product-icon">
              <img class="img-fluid" src="assets/images/pulse-product-icon.png"
                alt="Pulse - Africa's Leading Digital Publishing" />
            </div>
            <!-- / Product Icon -->

            <!-- Product Name -->
            <div class="product-name">
              <h1>
                Pulse
              </h1>
              <p>
                Digital Media Publisher
              </p>
            </div>
            <!-- / Product Name -->

            <!-- Mobile View Description -->
            <article class="d-lg-none">
              <p>
                Africa’s leading new media publisher reaching Africa’s mass mobile population with innovative,
                insightful
                and entertaining content across online platforms.
              </p>

              <a class="text-center" href="/pages/products.php">Explore in detail</a>
            </article>
            <!-- / Mobile View Description -->
          </div>
          <!-- / Product -->

          <!-- Alternate Product -- Shows only on hover and doesn't show on mobile -->
          <div class="product pulse active">

            <!-- Product Image -->
            <div class="product-image">
              <img class="img-fluid" src="assets/images/pulse-logo-white.png" alt="Pulse" />
            </div>
            <!-- / Product Image -->

            <!-- Description -->
            <article class="">
              <p>
                Africa’s leading new media publisher reaching Africa’s mass mobile population with innovative,
                insightful
                and entertaining content across online platforms.
              </p>

              <a class="text-center stretched-link" href="/pages/products.php">Explore in detail</a>
            </article>
          </div>
          <!-- / Alternate Product -->
        </div>

      </div>
      <!-- / Product Column -->

      <!-- Product Column -->
      <div class="col-md-4">

        <div class="position-relative h-100">
          <!-- Product -- Shows extra detail on hover only on desktop -->
          <div class="product play">

            <!-- Product Image -->
            <div class="product-image">
              <img class="img-fluid" src="assets/images/play-studio-logo.png" alt="Pulse" />
            </div>
            <!-- / Product Image -->

            <!-- Product Icon -->
            <div class="product-icon">
              <img class="img-fluid" src="assets/images/play-product-icon.png" alt="Play - Creative Content Studio" />
            </div>
            <!-- / Product Icon -->

            <!-- Product Name -->
            <div class="product-name">
              <h1>
                Play
              </h1>
              <p>
                Creative Content Studio
              </p>
            </div>
            <!-- / Product Name -->

            <!-- Mobile View Description -->
            <article class="d-lg-none">
              <p>
                Your full-scale digital studio for unrivalled visual content production supported by our vast media
                distibution network.
              </p>

              <a class="text-center" href="/pages/products.php">Explore in detail</a>
            </article>
            <!-- / Mobile View Description -->
          </div>
          <!-- / Product -->

          <!-- Alternate Product -- Shows only on hover and doesn't show on mobile -->
          <div class="product play active">

            <!-- Product Image -->
            <div class="product-image">
              <img class="img-fluid" src="assets/images/play-studio-logo-white.png"
                alt="Play - Creative Content Studio" />
            </div>
            <!-- / Product Image -->

            <!-- Description -->
            <article class="">
              <p>
                Africa’s leading new media publisher reaching Africa’s mass mobile population with innovative,
                insightful
                and entertaining content across online platforms.
              </p>

              <a class="text-center stretched-link" href="/pages/products.php">Explore in detail</a>
            </article>
          </div>
          <!-- / Alternate Product -->
        </div>

      </div>
      <!-- / Product Column -->

      <!-- Product Column -->
      <div class="col-md-4">

        <div class="position-relative h-100">
          <!-- Product -- Shows extra detail on hover only on desktop -->
          <div class="product rdm">

            <!-- Product Image -->
            <div class="product-image">
              <img class="img-fluid" src="assets/images/rdm-logo.png" alt="Pulse" />
            </div>
            <!-- / Product Image -->

            <!-- Product Icon -->
            <div class="product-icon">
              <img class="img-fluid" src="assets/images/rdm-product-icon.png"
                alt="RDM - Integrated Digital Solutions" />
            </div>
            <!-- / Product Icon -->

            <!-- Product Name -->
            <div class="product-name">
              <h1>
                Ringier Digital Marketing
              </h1>
              <p>
                Integrated Digital Solutions
              </p>
            </div>
            <!-- / Product Name -->

            <!-- Mobile View Description -->
            <article class="d-lg-none">
              <p>
                Your dedicated digital partner providing digital-first marketing and digital enterprise solutions to
                help you achieve your marketing and digital objectives.
              </p>

              <a class="text-center" href="/pages/products.php">Explore in detail</a>
            </article>
            <!-- / Mobile View Description -->
          </div>
          <!-- / Product -->

          <!-- Alternate Product -- Shows only on hover and doesn't show on mobile -->
          <div class="product rdm active">

            <!-- Product Image -->
            <div class="product-image">
              <img class="img-fluid" src="assets/images/rdm-logo-white.png" alt="RDM - Integrated Digital Solutions" />
            </div>
            <!-- / Product Image -->

            <!-- Description -->
            <article class="">
              <p>
                Your dedicated digital partner providing digital-first marketing and digital enterprise solutions to
                help you achieve your marketing and digital objectives.
              </p>

              <a class="text-center stretched-link" href="/pages/products.php">Explore in detail</a>
            </article>
          </div>
          <!-- / Alternate Product -->
        </div>

      </div>
      <!-- / Product Column -->

    </div>
    <!-- / Products -->
  </div>
</section>
<!-- / Product Overview -->

<!-- Brands Overview-->
<section class="bg-white section-padding">
  <div class="container">

    <!-- Header -->
    <h1 class="heading">
      BRANDS THAT TRUST US
    </h1>
    <!-- / Header -->

    <!-- Header Description -->
    <article class="heading-description">
      From globally recognized companies to local startups, our clients have something in common - they are happier with
      us.
    </article>
    <!-- / Header Description -->

    <div class="col-md-11 mx-auto">

      <!-- Brands -->
      <div class="row brands">

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/nestle-logo.png" alt="Nestle" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/durex-logo.png" alt="Durex" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/dettol-logo.png" alt="Dettol" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/dangote-logo.png" alt="Dangote" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/heineken-logo.png" alt="Heineken" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/siemens-logo.png" alt="Siemens" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/british-council-logo.png" alt="British Council" />
        </brand>
        <!-- / Brand -->

        <!-- Brand -->
        <brand>
          <img class="img-fluid" src="assets/images/zenith-bank-logo.png" alt="Zenith Bank" />
        </brand>
        <!-- / Brand -->

      </div>
      <!-- / Brands -->

    </div>

  </div>

  <div class="text-center">
    <a href="/pages/brands-archive.php">More clients</a>
  </div>

</section>
<!-- / Brands Overview-->

<!-- Client Feedback -->
<section class="section-padding bg-white">

  <div class="clients-feedback-section">
    <div class="container">

      <!-- Feedbacks -- Each <feedback> turns into a carousel -->
      <client-feedbacks>

        <!-- Feedback -->
        <feedback class="carousel-cell">

          <h1 class="heading">
            What clients are saying
          </h1>

          <!-- Client Comment -->
          <article class="comment">
            <p>
              “I had a fantastic experience working with you! Extremely professional, attention to detail and quick to
              respond to feedbacks. I look forward to more partnerships.”
            </p>
          </article>
          <!-- / Client Comment -->

          <!-- Client -->
          <client>
            <p class="name">
              Obagun Jaja
            </p>

            <p class="company">
              Reckitt Benckiser
            </p>
          </client>
          <!-- / Client -->

        </feedback>
        <!-- / Feedback -->

        <!-- Feedback -->
        <feedback class="carousel-cell">

          <h1 class="heading">
            What clients are saying
          </h1>

          <!-- Client Comment -->
          <article class="comment">
            <p>
              Testing 2
            </p>
          </article>
          <!-- / Client Comment -->

          <!-- Client -->
          <client>
            <p class="name">
              Edward Bella
            </p>

            <p class="company">
              Reckitt Benckiser
            </p>
          </client>
          <!-- / Client -->

        </feedback>
        <!-- / Feedback -->

      </client-feedbacks>
      <!-- / Feedbacks -->

    </div>
  </div>

</section>
<!-- / Client Feedback -->

<!-- Contact Section -->
<section class="section-padding bg-white">

  <div class="container">

    <h1 class="heading">
      Got a project? We're ready!
    </h1>

    <!-- Block Button -->
    <div class="text-center">
      <a class="btn-contact waves-effect waves-dark" href="pages/contact.php">Contact Us</a>
    </div>
    <!-- / Block Button -->

  </div>

</section>
<!-- / Contact Section -->

<?php include "inc/footer.php";?>