// Scripts that will be executed once the Window is starts loading
$(window).on("load", function () {});

// Read comments carefully

// Scripts that will be executed once the DOM is loaded fully
$(document).ready(function () {

  //Reload the browser on window resize
  var resizeId;
  $(window).resize(function () {
    clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 500);
  });

  function doneResizing() {
    location.reload(); //whatever we want to do
  }

  //Set Page padding under the header and also modify mobile menu
  function setPagePadding() {
    let hMenu = document.getElementById("menu"); // Get Menu header ID
    let getHeight = hMenu.offsetHeight; //Get Menu Header height
    console.log(getHeight + "px is the height of the menu bar"); // Console Log for error
    var section = document.querySelector("section"); //Get Next "section" tag below the header
    let mobileMenu = document.getElementById("side-menu"); //Get Mobile Menu ID
    let hello = window.screen.height; //Get View Screen Height
    let world = window.screen.width; //Get View Screen WIDTH
    console.log("Full Screen height is " + hello + "px");
    console.log("Full Screen width is " + world + "px");

    if (getHeight) {
      //section.style.paddingTop = getHeight + "px"; // Set the height of the Navbar to the same padding for the Next section
      console.log("Page Padding set successfully");
      section.style.height = "100vh"; //Set section below the Navbar with a full viewport height
    }

    mobileMenu.style.top = "-" + hello + "px"; // Set the offset for the Mobile menu
  }
  setPagePadding(); // Initialize the function

  //Initiate AOS animation library
  function animateAll() {
    AOS.init({
      duration: 1200,
      delay: 130,
      once: false,
      mirror: true
      //anchorPlacement: "top-center"
    });
  }
  animateAll();

  //Function that initiates the Progress bar page slider
  function progressMove() {
    var getMax = function () {
      return $(document).height() - ($(window).height() + window.screen.height);
    };

    var getValue = function () {
      return $(window).scrollTop();
    };

    if ("max" in document.createElement("progress")) {
      // Browser supports progress element
      var progressBar = $("progress");

      // Set the Max attr for the first time
      progressBar.attr({
        max: getMax()
      });

      $(document).on("scroll", function () {
        // On scroll only Value attr needs to be calculated
        progressBar.attr({
          value: getValue()
        });
      });

      $(window).resize(function () {
        // On resize, both Max/Value attr needs to be calculated
        progressBar.attr({
          max: getMax(),
          value: getValue()
        });
      });
    } else {
      var progressBar = $(".progress-bar"),
        max = getMax(),
        value,
        width;

      var getWidth = function () {
        // Calculate width in percentage
        value = getValue();
        width = (value / max) * 100;
        width = width + "%";
        return width;
      };

      var setWidth = function () {
        progressBar.css({
          width: getWidth()
        });
      };

      $(document).on("scroll", setWidth);
      $(window).on("resize", function () {
        // Need to reset the Max attr
        max = getMax();
        setWidth();
      });
    }
  }
  progressMove();

  //Initiate Sticky Menu
  function stickyMenu() {
    $(window).on("scroll", function () {
      // Get the navbar
      var navbar = document.getElementById("menu");
      var section = document.querySelector("section");

      // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
      if (
        document.body.scrollTop > 250 ||
        document.documentElement.scrollTop > 250
      ) {
        navbar.classList.add("sticky");
        //section.classList.add("sticky");
      } else {
        navbar.classList.remove("sticky");
        //section.classList.remove("sticky");
      }
    });
  }

  stickyMenu();

  //Initiate Back-to-top Button
  function backToTop() {
    // Back to top
    var amountScrolled = 350;
    var amountScrolledNav = 25;

    $(window).scroll(function () {
      if ($(window).scrollTop() > amountScrolled) {
        $("button.back-to-top").addClass("show");
      } else {
        $("button.back-to-top").removeClass("show");
      }
    });

    $("button.back-to-top").click(function () {
      $("html, body").animate({
          scrollTop: 0
        },
        800
      );
      return false;
    });
  }

  backToTop();

  //Initialize Flickty Carousel
  function flickItNow() {
    $(".main-slider-hero").flickity({
      // options
      prevNextButtons: false,
      fade: true,
      cellAlign: "left",
      contain: true,
      wrapAround: true,
      autoPlay: 7500,
      pauseAutoPlayOnHover: false,
      // Disable dragging
      draggable: false,
      pageDots: false
    });

    $(".case-studies").flickity({
      // options
      prevNextButtons: false,
      cellAlign: "left",
      contain: true,
      draggable: Modernizr.touchevents,
      wrapAround: false,
      autoPlay: false,
      pageDots: false
    });

    $("client-feedbacks").flickity({
      // options
      prevNextButtons: false,
      fade: true,
      cellAlign: "left",
      contain: true,
      wrapAround: false,
      autoPlay: 7500,
      pauseAutoPlayOnHover: false,
      // Disable dragging
      draggable: false,
      pageDots: false
    });

    /*$(".products").flickity({
      // options
      prevNextButtons: false,
      cellAlign: "left",
      contain: true,
      draggable: Modernizr.touchevents,
      wrapAround: false,
      autoPlay: false,
      pageDots: false
    });*/
  }
  flickItNow();

  // Find all links within the HTML document and create a title element for accessibility
  function printLinkTitles() {
    let queryA = document.querySelectorAll("a"); //Query all the Anchor Tags

    for (var i = 0; i < queryA.length; i++) {
      if (queryA[i].hasAttribute("title")) { // Check if the Anchor Tags have a title tag on it already
        console.log("Found some links with title tags already");
      } else {
        var elem = queryA[i];
        var att = document.createAttribute("title");
        att.value = queryA[i].innerText
        elem.setAttributeNode(att);
      }
    }
    //console.log("Creating titles for Links done Successfully");
    console.log(queryA[10].innerHTML);
  }
  printLinkTitles(); // RUn the function

  //Create Main Slider Social Links
  function sliderSocials() {

    var links = document.querySelectorAll("footer .social-widget a"); // Get the Social media links from the footer
    var index = document.getElementsByClassName("main-slider-hero"); // Select the Slider Hero
    var icons = ["fa-facebook", "fa-twitter", "fa-instagram", "fa-youtube", "fa-linkedin"]; // Array of Social Media icons
    var ul = document.createElement("ul"); // Create UL HTML Element
    ul.className = "social-widget"; // Add class to the created UL tag

    for (var i = 0; i < icons.length; i++) {

      if (icons[i] instanceof Array) { // for each item in the Icons Array; map them to a new LI Tag.
        var list = iconsToUl(icons[i]);
      } else {
        var li = document.createElement("li"); // Create <li> tag
        ul.appendChild(li); // Append the <li></li> to the ul parent
        //console.log(ul.appendChild(li));

        var a = document.createElement("a"); //Create a new <a> tag inside the <li> tag
        var span = document.createElement("i"); // Create <i> tag inside the <a></a> tag
        span.className = "fa " + icons[i]; // Add the fontawesome classes to the <i> tag.

        a.href = links[i]; // Map each link href gotten from the footer to the href of the created <a>
        a.appendChild(span); // Append the <i> to the a parent.
        li.appendChild(a); // Append the <a></a> to the li parent.
        //console.log(li.appendChild(a)); 
      }
      index[0].appendChild(ul);
    }
  }
  sliderSocials();
});