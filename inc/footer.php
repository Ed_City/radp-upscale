<footer>

  <div class="container">

    <!-- Row -->
    <div class="row justify-content-between mb-4">

      <div class="col-lg-4 col-md-5">

        <!-- Footer Content -->
        <article class="footer-content">
          <figure>
            <img class="img-fluid" src="assets/images/radp-logo.png" alt="Ringier Africa Digital Publishing Logo" />
          </figure>

          <h4>
            <strong>We are your partner</strong>, here to make digital work for you!
          </h4>
        </article>
        <!-- / Footer Content -->

      </div>

      <div class="col-lg-6 col-md-7">

        <div class="row">

          <!-- Column -->
          <div class="col">
            <!-- Footer Links Column -->
            <article class="footer-links">
              <p class="menu-head">Offices</p>

              <ul>
                <li>
                  <a href="pages/contact.php">Nigeria</a>
                </li>
                <li>
                  <a href="pages/contact.php">Ghana</a>
                </li>
                <li>
                  <a href="pages/contact.php">Kenya</a>
                </li>
                <li>
                  <a href="pages/contact.php">Senegal</a>
                </li>
              </ul>
            </article>
            <!-- / Footer Links Column -->
          </div>
          <!-- / Column -->

          <!-- Column -->
          <div class="col">
            <!-- Footer Links Column -->
            <article class="footer-links">
              <p class="menu-head">Outlets</p>

              <ul>
                <li>
                  <a href="pages/products.php">Pulse</a>
                </li>
                <li>
                  <a href="pages/products.php">Play Studio</a>
                </li>
                <li>
                  <a href="pages/products.php">RDM</a>
                </li>
              </ul>
            </article>
            <!-- / Footer Links Column -->
          </div>
          <!-- / Column -->

          <!-- Column -->
          <div class="col">
            <!-- Footer Links Column -->
            <article class="footer-links">
              <p class="menu-head">Follow Us:</p>

              <ul class="social-widget">
                <li>
                  <a href="https://facebook.com"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a href="#twitter"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a href="#instagram"><i class="fa fa-instagram"></i></a>
                </li>
                <li>
                  <a href="#youtube"><i class="fa fa-youtube"></i></a>
                </li>
                <li>
                  <a href="#linked-in"><i class="fa fa-linkedin"></i></a>
                </li>
              </ul>
            </article>
            <!-- / Footer Links Column -->
          </div>
          <!-- / Column -->

        </div>

      </div>

    </div>
    <!-- /Row -->

    <!-- Row -->
    <div class="row justify-content-between ">

      <div class="col-lg-4 col-md-5">

        <!-- Footer Note Content -->
        <article class="footer-content footnote">
          <strong>
            <script>
              var CurrentYear = new Date().getFullYear()
              document.write(CurrentYear)
            </script> Ringier Africa Digital Publishing.
          </strong> All rights reserved.
        </article>
        <!-- / Footer Note Content -->

      </div>

      <div class="col-lg-6 col-md-7">
        <article class="footnote-links">
          <ul>
            <li>
              <a href="pages/general-content.php">
                Privacy policy
              </a>
            </li>
            <li>
              <a href="pages/general-content.php">
                Cookie policy
              </a>
            </li>
            <li>
              <a href="pages/general-content.php">
                Terms and Conditions
              </a>
            </li>
          </ul>
        </article>
      </div>

    </div>
    <!-- /Row -->

  </div>

</footer>


<?php include "scripts.php"; ?>

</body>

</html>