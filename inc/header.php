<!DOCTYPE html>
<html lang="en">

  <head>
    <title>
      RADP - Ringier Africa Digital Publishing
    </title>

    <!-- Charset -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <!-- Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no" />
    <!-- Meta Tags -->

    <!-- CSS -->

    <!-- Preloaded Links -->
    <link rel="preconnect" href="https://unpkg.com" />
    <link rel="preconnect" href="https://instant.page" />
    <link rel="preconnect" href="https://fonts.googleapis.com/" />

    <link rel="shortcut icon" href="assets/images/favicon.png" />

    <!-- Flickity CSS -->
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/flickity-fade@1/flickity-fade.css">

    <!-- FontAwesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    <!-- AOS CSS -->
    <link href="https://unpkg.com/aos@next/dist/aos.css" rel="stylesheet" />

    <!-- Better Nav -->
    <link rel="stylesheet" href="assets/css/better-nav/bootstrap-better-nav.min.css" />

    <!-- Style CSS -->
    <link media="all" rel="stylesheet" href="assets/css/style.css" />
  </head>

  <body data-instant-allow-query-string data-instant-allow-external-links>
    <!-- Page Progress Transition -->
    <progress value="0"></progress>
    <!-- / Page Progress Transition -->

    <header id="menu" class="">
      <nav class="navbar navbar-expand-lg better-bootstrap-nav-left">
        <a class="navbar-brand" href="index.php" data-aos="fade" data-aos-mirror="false">
          <img class="logo img-fluid" src="assets/images/logo-white.png" />
        </a>
        <!-- / Brand Logo -->

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#view" aria-controls="view"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="radp-toggler"></span>
          <span class="radp-toggler"></span>
          <span class="radp-toggler"></span>
        </button>

        <!-- Central Menu -->
        <div id="view" class=" collapse navbar-collapse" data-aos="zoom-in" data-aos-mirror="false">
          <div class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link smooth-scroll" href="#catalogue">Case Studies</a>
            </li>

            <li class="nav-item">
              <a class="nav-link smooth-scroll" href="#story">Products</a>
            </li>

            <li class="nav-item">
              <a class="nav-link smooth-scroll" href="#contact">Team</a>
            </li>

            <li class="nav-item">
              <a class="nav-link smooth-scroll" href="#contact">Contact Us</a>
            </li>
          </div>
          <!-- / Central Menu -->

          <form action="" method="post" id="search" class="form-inline">
            <div class="input-group">
              <input type="search" class="form-control" placeholder="Search our site" aria-label="Search bar"
                aria-describedby="Search Here" autocomplete="off" />
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-search"></i></span>
              </div>
            </div>
          </form>
        </div>
      </nav>
    </header>