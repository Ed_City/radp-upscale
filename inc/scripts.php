<script src="assets/js/instantpage.js" type="module" defer></script>

<!--Jquery JS-->
<script src="assets/js/jquery.js"></script>

<!-- Popper JS -->
<script src="assets/js/popper.min.js" crossorigin="anonymous"></script>

<!--Bootstrap JS-->
<script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>

<!--Bootstrap Better Nav JS-->
<script src="assets/js/bootstrap-better-nav.min.js" crossorigin="anonymous"></script>

<!--Waves JS-->
<script src="assets/js/waves.js"></script>

<!--Modernizr JS-->
<script src="assets/js/modernizr-custom.js"></script>

<!-- AOS -->
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>

<!--Smooth Scroll JS-->
<script src="assets/js/smoothscroll.js"></script>

<!-- Flickity JavaScript -->
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="https://unpkg.com/flickity-fade@1/flickity-fade.js"></script>

<!-- App Js -->
<script src="assets/js/app.js"></script>

