# RADP Upscale

This is a private repository consisting of files that would be used for the CMS development.

![screenshot image](screenshot.jpg)

The files contain template files of how page elements would be rendered upon successful integration with the CMS and live preview.

## Installation

Download the master zip file and extract into your local development network.

You can use popular local development servers like XAMP, MAMP or Laravel Valet

## Usage

Everything should work as is. Tweak at your own time to modify or add new features.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
